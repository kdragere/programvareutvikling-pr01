import org.junit.Test;

import java.util.ResourceBundle;

import static org.junit.Assert.*;

/**
 * This test class checks that i18n is in order for both Norwegian and English.
 * @author Per-Kristian
 */
public class InternationalizationTest {
    @Test
    public void testInternationalization(){


        Internationalization i18n = new Internationalization("no", "NO");
        ResourceBundle messages = i18n.getLang();
        String testCancel = messages.getString("buttonCancel");
        assertEquals("Avbryt", testCancel);

        Internationalization i18n2 = new Internationalization("en", "US");
        messages = i18n2.getLang();

        testCancel = messages.getString("buttonCancel");
        assertEquals("Cancel", testCancel);
    }
}
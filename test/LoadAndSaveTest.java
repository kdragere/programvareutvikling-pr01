import com.sun.xml.internal.fastinfoset.util.StringArray;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * This is a test for assuring that saving and loading files works. Is checks the functionality of the
 * LoadAndSave class. It creates a test file, and will also fail if the file can't be deleted, so that the computer
 * won't be filled up with old test files.
 * @author Per-Kristian
 */
public class LoadAndSaveTest {

    @Test
    public void testSaveToFile() throws Exception {
        List<Object> saveList = new ArrayList<Object>();
        List<Object> loadList;
        String string1 = "hello";
        String string2 = "world";

        saveList.add(0, string1);
        saveList.add(1, string2);
        LoadAndSave.saveToFile("test.txt", saveList);

        loadList = (List<Object>) LoadAndSave.loadFromFile("test.txt");
        assertEquals(loadList.get(0), saveList.get(0));
        assertEquals(loadList.get(1), saveList.get(1));

        boolean success = new File("test.txt").delete();
        if (!success){
            fail("Couldn't delete the file");
        }

    }
}
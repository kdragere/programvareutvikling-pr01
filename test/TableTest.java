import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by jdr on 11/10/15.
 */
public class TableTest extends TestCase {

    @Test
    public void testRowOperations() throws Exception {
        Internationalization inter = new Internationalization("no", "norway");
        Table table = new Table(inter);

        assertEquals(0, table.getList().size());
        table.addRow();
        table.addRow();
        assertEquals(2, table.getList().size());
        table.addRow();
        table.clear();
        assertEquals(0, table.getList().size());
    }

    @Test
    public void testModel() throws Exception {
        MyTableModel model = new MyTableModel();

        model.addRow();
        model.setValueAt(Constants.JLABEL, 0, 0);
        assertEquals(Constants.JLABEL, model.getValueAt(0,0));
        model.setValueAt(Constants.JTEXTAREA, 0, 0);
        assertEquals(Constants.JTEXTAREA, model.getValueAt(0,0));
    }
}

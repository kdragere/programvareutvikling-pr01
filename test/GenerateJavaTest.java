import junit.framework.TestCase;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jdr on 10/10/15.
 */
public class GenerateJavaTest extends TestCase {

    @Test
    public void testMakeCode() throws Exception {
        ArrayList<String[]> valuesList = new ArrayList<>();

        String[] str1 = {Constants.JLABEL, "var1", "test1", "1", "1", "1", "1", "1", "1"};
        String[] str2 = {Constants.JTEXTFIELD, "var2", "test2", "1", "1", "1", "1", "2", "1","2"};
        String[] str3 = {Constants.JTEXTAREA, "var3", "test2", "1", "1", "1", "1", "1", "2","2", "3"};
        valuesList.add(str1);
        valuesList.add(str2);
        valuesList.add(str3);
        GenerateJava.MakeCode(valuesList, "./out/production/test");
    }
}
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains functions for loading and saving layouts from/to files.
 * @author Per-Kristian Nilsen
 * Date: 02.10.2015
 */
public class LoadAndSave {

    /**
     * This function handles saving a layout to file. It takes a filename and a list of objects as parameters.
     * The list of objects will simply be written to the filename specified in the parameter.
     * @param filename the file name
     * @param list the list of objects
     * @return "success" if it's successful, null if not
     */
    public static String saveToFile(String filename, List<Object> list){
        try {
            FileOutputStream fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
            oos.close();
            fos.close();
            return "success";
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This function handles loading a layout from a file. It takes a file name as parameter, and will load the
     * layout from that specific file. The function will return a list of objects.
     * @param filename the file name
     * @return a list of objects
     */
    public static Object loadFromFile(String filename){
        try {
            FileInputStream fis = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            List list;
            list = (List) ois.readObject();
            ois.close();
            fis.close();
            return list;
        } catch (EOFException e){
            // do nothing
        } catch (IOException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        return null;
    }
}

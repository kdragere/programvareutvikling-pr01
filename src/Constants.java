/**
 * Created by jdr on 02/10/15.
 */

public class Constants {
    final static String JLABEL = "JLabel";
    final static String JTEXTFIELD = "JTextField";
    final static String JBUTTON = "JButton";
    final static String JTEXTAREA= "JTextArea";
}
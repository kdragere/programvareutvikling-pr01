import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;

/**
 * This class handles editing properties of a specific row. It creates a small editor, and the appearance changes
 * slightly depending on the type of object to edit. This editor is opened by right clicking on a row and selecting
 * properties.
 * @author Per-Kristian Nilsen
 * Date: 10.10.2015
 */
public class PropertyEditor {

    /**
     * This function is a static function that handles everything in the property editor. It uses actionListeners for
     * the buttons, which either close the editor, or adds the properties to a row by calling another function.
     * @param type the type of object to edit
     * @param table instance of the table calling the function
     * @param row the row to be edited
     * @param i18n object for internationalization
     */
    public static void getProperties(String type, MyTableModel table, int row, Internationalization i18n) {
        if (!type.equals(Constants.JTEXTAREA) && !type.equals(Constants.JTEXTFIELD)) {
            // do nothing
        } else {
            ResourceBundle messages;

            // Get the correct language for internationalization
            messages = i18n.getLang();

            JLabel rowLabel = new JLabel(messages.getString("tableRows"));
            JLabel columnLabel = new JLabel(messages.getString("tableColums"));

            JSpinner columns = new JSpinner();
            JSpinner rows = new JSpinner();
            JFrame frame = new JFrame(messages.getString("propertyFrameTitle"));

            frame.setLayout(new BorderLayout());

            // Panel for labels and spinners
            JPanel inputPanel = new JPanel();
            inputPanel.setLayout(new GridLayout(2, 2));

            inputPanel.add(columnLabel);

            // Get existing property values
            int[] props = table.getExtra(row);

            columns.setValue(props[0]);
            inputPanel.add(columns);

            // If the type of object is a JTextArea, let user edit rows
            if (type.equals(Constants.JTEXTAREA)) {
                inputPanel.add(rowLabel);

                rows.setValue(props[1]);
                inputPanel.add(rows);
            }

            // Create OK and Cancel button, and add them to the buttonPanel
            JButton okButton = new JButton(messages.getString("buttonOk"));
            JButton cancelButton = new JButton(messages.getString("buttonCancel"));

            JPanel buttonPanel = new JPanel();
            buttonPanel.add(okButton);
            buttonPanel.add(cancelButton);

            okButton.addActionListener(new ActionListener() {
                /**
                 * When the user presses the okButton, the properties will be added to the row.
                 * @param e the action performed
                 */
                @Override
                public void actionPerformed(ActionEvent e) {
                    int rowResult = (Integer) rows.getValue();
                    int columnResult = (Integer) columns.getValue();

                    // Checks the type again and makes sure the values are legal, before sending the values.
                    if (type.equals(Constants.JTEXTAREA) && rowResult >= 0 && columnResult >= 0) {
                        int[] res = {columnResult, rowResult};
                        frame.dispose();
                        table.setExtra(row, res);
                    } else if (type.equals(Constants.JTEXTFIELD) && columnResult >= 0) {
                        int[] res = {columnResult};
                        frame.dispose();
                        table.setExtra(row, res);
                    } else {
                        JOptionPane.showMessageDialog(frame, messages.getString("illegalValues"));
                    }
                }
            });

            cancelButton.addActionListener(new ActionListener() {
                /**
                 * Makes the cancel button close the property window.
                 * @param e the action performed
                 */
                @Override
                public void actionPerformed(ActionEvent e) {
                    frame.dispose();
                }
            });

            // Add the buttonPanel and inputPanel to the JFrame.
            frame.add("South", buttonPanel);

            frame.add("North", inputPanel);
            frame.pack();
            frame.setVisible(true);
        }
    }
}
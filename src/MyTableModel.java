import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.io.Serializable;
import java.util.*;

/**
 * This is the model class for the table. Contains row objects and handles changes, output, input to them.
 * @author Jardar Tøn
 * Date: 02.10.2015
 */
public class MyTableModel extends AbstractTableModel {

    Internationalization internationalization = new Internationalization(System.getProperty("user.language"), System.getProperty("user.country"));
    ResourceBundle messages=internationalization.getLang();

    public String[] columnNames = new String[] {
            messages.getString("tableType"),
            messages.getString("tableName"),
            messages.getString("tableText"),
            messages.getString("tableRow"),
            messages.getString("tableColumn"),
            messages.getString("tableRows"),
            messages.getString("tableColums"),
            messages.getString("tableFill"),
            messages.getString("tableAnchor")};

    private List<Row> data = new ArrayList<Row>();

    private int i = 0;

    public MyTableModel() {
    }

    /**
     * Sets the extra values for the row. Like columns for JTextField
     * @param row the row to set the extra values for
     * @param values the values to set
     */
    public void setExtra(int row, int[] values) {
        data.get(row).extraValues.clear();
        for (int val : values) {
            data.get(row).extraValues.add(val);
        }
    }

    /**
     * Gets the extra values from a row.
     * @param row the row to get the data from
     * @return int array containing the values or a array containing zeros.
     */
    public int[] getExtra(int row) {
        int[] arr = {0, 0};
        if (data.get(row).extraValues.isEmpty())
            return arr;
        for (i = 0; i < 2; i++ ) {
              arr[i] = data.get(row).extraValues.get(i);
        }
        return arr;
    }

    /**
     * Rotates the elements of the list.
     * @param selectedRow the row the user wants to move.
     */
    public void moveRowsUp(int selectedRow) {
            Collections.swap(data, selectedRow, selectedRow-1);
    }

    public void moveRowsDown(int selectedRow) {
            Collections.swap(data, selectedRow, selectedRow+1);
    }

    /**
     * Gets all rows and their extra values.
     * @return a arraylist containing string arrays with all row values plus their extra values.
     */
    public ArrayList<String[]> getAllValues() {
        ArrayList<String[]> table= new ArrayList<>();
        for (Row element: data) {
            // if has extra values add them to the array for the row
            if (!element.extraValues.isEmpty()) {
                ArrayList<String> allValues = new ArrayList(Arrays.asList(element.values));
                for (int extval : element.extraValues) {
                   allValues.add(String.valueOf(extval));
                }
                String[] arr = new String[allValues.size()];
                table.add(allValues.toArray(arr));
            }
            // else just add the row values.
            else {
                table.add(element.values);
            }
        }
        return table;
    }

    /**
     * Setter function for the data list.
     * @param list a arraylist containing row objects.
     */
    public void setList(List list){
        data = list;
    }

    /**
     * Adds a new row to the list
     */
    public void addRow() {
        data.add(new Row(++i));
    }

    /**
     * Removes a row from the list
     * @param row the row to be removed
     */
    public void removeRow(int row) {
        data.remove(row);
    }

    /**
     * gets the list.
     * @return the datalist containing all the rows.
     */
    public List getRowList() {
        return data;
    }

    /**
     * Removes all rows from the list
     */
    public void removeAll() {
        data.clear();
    }

    /**
     * gets the size of the list
     * @return the number of rows in the list
     */
    @Override
    public int getRowCount() {
        return data.size();
    }

    /**
     * gets the name (header) of a column
     * @param col the number of the column
     * @return the name of the column
     */
    @Override
    public String getColumnName(int col) { return columnNames[col];}

    /**
     * Gets the number of columns.
     * @return 9 columns
     */
    @Override
    public int getColumnCount() {
        return 9;
    }

    /**
     * Gets the value of one field. on column in one row.
     * @param row the row to get the value from
     * @param col the col to get the value from.
     * @return the value in the the selected field.
     */
    @Override
    public Object getValueAt(int row, int col) {
        if (col > 7)
            return Integer.parseInt(data.get(row).values[col]);
        return data.get(row).values[col];
    }

    /**
     * Sets the value when a field is edited.
     * @param aValue the value to be saved.
     * @param row the row to set the value of
     * @param col the column to set the value at
     */
    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (aValue instanceof Integer)
            aValue = aValue.toString();
        data.get(row).values[col] = (String) aValue;
        fireTableCellUpdated(row, col);
    }

    /**
     * Returns true so cells can be edited.
     * @return true
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    /**
     * gets the type of class for the column. Used for cell rendering.
     * @return string for all columns less then 7 and icon for those two over 6
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex > 6)
            return Icon.class;
        else
           return String.class;
    }

    /**
     * Static class row is the class for the objects representing rows in the tableModel.
     * @author Jardar Tøn
     * Date: 02.10.2015
     */
    private static class Row implements Serializable {

        public String[] values = {Constants.JLABEL, "navn", "text","1","1","1","1","0","0"};
        public ArrayList<Integer> extraValues = new ArrayList<>();

        /**
         * gives values different default names
         * @param i used to give rows different default names
         */
        public Row(int i) {
            values[1] += i;
            values[2] += i;
        }
    }
}

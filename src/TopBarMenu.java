//import com.sun.tools.javac.jvm.Gen;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.List;

/**
 * Created by Kristian on 02.10.2015.
 * @author Kristian, Per-Kristian
 */
public class TopBarMenu {
    Internationalization internationalization;
    ResourceBundle messages;
    FileOpener fileOpener;
    Editor editor;
    BorderLayout border;
    JPanel panel;
    Table table;

    public TopBarMenu(Editor editor, BorderLayout border, JPanel panel, Internationalization internationalization, Table table) {
        this.editor = editor;
        this.border = border;
        this.panel = panel;
        this.internationalization = internationalization;
        this.table = table;
    }

    public JMenuBar createTopBarMenu() {
        messages = internationalization.getLang();
        fileOpener = new FileOpener(internationalization);
        URL iconUrl;

        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu(messages.getString("menuFile"));
        JMenu change = new JMenu(messages.getString("menuOptions"));
        JMenu help = new JMenu(messages.getString("menuHelp"));

        iconUrl = getClass().getResource("icons/NEW.GIF");
        JMenuItem menuFileNew = new JMenuItem(messages.getString("menuFileNew"), new ImageIcon(iconUrl));
        menuFileNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.table.clear();
                panel.repaint();
            }
        });
        file.add(menuFileNew);

        iconUrl = getClass().getResource("icons/OPENDOC.GIF");
        JMenuItem menuFileOpen = new JMenuItem(messages.getString("menuFileOpen"), new ImageIcon(iconUrl));
        menuFileOpen.addActionListener(new ActionListener() {
            /**
             * When the user clicks Open, a FileChooser lets them choose a file to load.
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                // Get the selected file from FileChooser
                String fileName = fileOpener.open();
                if (fileName != null) {
                    // Load the layout from the file
                    editor.table.loadTable((List) LoadAndSave.loadFromFile(fileName));
                    panel.repaint();
                    editor.currentFile = fileName;
                }
            }
        });
        file.add(menuFileOpen);

        iconUrl = getClass().getResource("icons/SAVE.GIF");
        JMenuItem menuFileSave = new JMenuItem(messages.getString("menuFileSave"), new ImageIcon(iconUrl));
        menuFileSave.addActionListener(new ActionListener() {
            /**
             * When the user clicks Save, and they are working on an existing file, they will overwrite that file.
             * If they are working on a new file, the save button will act as a "Save as.." button, displaying a
             * FileChooser.
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                // If the user is working on a new file, open FileChooser for saving
                if (editor.currentFile == "") {
                    String fileName = fileOpener.save(null);
                    if (fileName != null) {
                        fileName = fileName + ".ezl";
                        LoadAndSave.saveToFile(fileName, editor.table.getList());
                        editor.currentFile = fileName;
                    }
                } else {
                    // If working on an existing file, overwrite that file
                    LoadAndSave.saveToFile(editor.currentFile, editor.table.getList());
                }
            }
        });
        file.add(menuFileSave);

        iconUrl = getClass().getResource("icons/SAVE.GIF");
        JMenuItem menuFileSaveAs = new JMenuItem(messages.getString("menuFileSaveAs"), new ImageIcon(iconUrl));
        menuFileSaveAs.addActionListener(new ActionListener() {
            /**
             * When the user clicks Save As, a FileChooser will let them choose where to save their layout.
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                // Open FileChooser for saving and update currentFile.
                String fileName = fileOpener.save(null);
                if (fileName != null) {
                    fileName = fileName + ".ezl";
                    LoadAndSave.saveToFile(fileName, editor.table.getList());
                    editor.currentFile = fileName;
                }
            }
        });
        file.add(menuFileSaveAs);
        file.add(new JSeparator());

        iconUrl = getClass().getResource("icons/ExecuteProject.gif");
        JMenuItem menuFilePreview = new JMenuItem(messages.getString("menuFilePreview"), new ImageIcon(iconUrl));
        menuFilePreview.addActionListener(new ActionListener() {
            /**
             * When the user clicks Preview, a preview of the code will be generated
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                GenerateJava.MakeCode(editor.table.getAllRows(), null);
            }
        });
        file.add(menuFilePreview);

        iconUrl = getClass().getResource("icons/SAVEJAVA.GIF");
        JMenuItem menuFileGenerate = new JMenuItem(messages.getString("menuFileGenerate"), new ImageIcon(iconUrl));
        menuFileGenerate.addActionListener(new ActionListener() {
            @Override
            /**
             * When the user clicks Generate, the fileOpener\explorer will open so the user can choose where to save
             * generated code as a java file.
             * @param e the action performed
             */
            public void actionPerformed(ActionEvent e) {
                String fileName = fileOpener.save("java");
                if (fileName != null) {
                    GenerateJava.MakeCode(editor.table.getAllRows(), fileName);
                }
            }
        });
        file.add(menuFileGenerate);
        file.add(new JSeparator());

        iconUrl = getClass().getResource("icons/close.gif");
        JMenuItem menuFileClose = new JMenuItem(messages.getString("menuFileClose"), new ImageIcon(iconUrl));
        menuFileClose.addActionListener(new ActionListener() {
            @Override
            /**
             * When the user clicks Close, The program will shut down
             * @param e the action performed
             */
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        file.add(menuFileClose);

        iconUrl = getClass().getResource("icons/NEWROW.GIF");
        JMenuItem menuChangeNewRow = new JMenuItem(messages.getString("menuChangeNewRow"), new ImageIcon(iconUrl));
        menuChangeNewRow.addActionListener(new ActionListener() {
            /**
             * When the user clicks NewRow, a new row will appear in the table
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.table.addRow();
                panel.repaint();
            }
        });
        change.add(menuChangeNewRow);

        iconUrl = getClass().getResource("icons/close.gif");
        JMenuItem menuChangeDeleteRow = new JMenuItem(messages.getString("menuChangeDeleteRow"), new ImageIcon(iconUrl));
        menuChangeDeleteRow.addActionListener(new ActionListener() {
            /**
             * When the user clicks DelteRow, the selected row will be deleted from the table
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if (editor.table.rowCount() > 0 && editor.table.selectedRow > -1
                        && editor.table.selectedRow < editor.table.rowCount()) {
                    editor.table.removeRow(table.selectedRow);
                    panel.repaint();
                }
            }
        });
        change.add(menuChangeDeleteRow);

        iconUrl = getClass().getResource("icons/HELP.GIF");
        JMenuItem menuHelpHelp = new JMenuItem(messages.getString("menuHelpHelp"), new ImageIcon(iconUrl));
        menuHelpHelp.addActionListener(new ActionListener() {
            /**
             * When the user clicks Help, a message dialog will appear. The help section is not implemented
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, messages.getString("menuHelpHelpTxt"), messages.getString("menuHelpHelp"), JOptionPane.INFORMATION_MESSAGE);
            }
        });
        help.add(menuHelpHelp);
        help.add(new JSeparator());

        JMenuItem menuHelpAbout = new JMenuItem(messages.getString("menuHelpAbout"));
        menuHelpAbout.addActionListener(new ActionListener() {
            /**
             * When the user clicks About, a message dialog will appear.
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, messages.getString("menuHelpAboutTxt") + ":\n\n Kristian\n Jardar\n Per-Kristian", messages.getString("menuHelpAbout") + ":", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        help.add(menuHelpAbout);

        menuBar.add(file);
        menuBar.add(change);
        menuBar.add(help);
        return menuBar;
    }
}

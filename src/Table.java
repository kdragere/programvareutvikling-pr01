import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * This class handles all the operations on the table object and the underlying model of the table.
 * @author Jardar Tøn, Per-Kristian Nilsen
 * Date: 02.10.2015
 */
public class Table {
    Internationalization internationalization;
    ResourceBundle messages;
    private JTable table;
    private JScrollPane jScrollPane;
    private MyTableModel tableModel;
    private ArrayList<ImageIcon[]> images;
    private String[] imageNames;
    public int selectedRow;

    /**
     * Initalizes the table, model, scrollPane objects.
     * Sets up the renderes for comboboxes with and without images, and defalt CellRenderers.
     * @param internationalization the internatonalization object.
     */
    public Table(Internationalization internationalization) {
        this.internationalization = internationalization;
        messages = internationalization.getLang();
        tableModel = new MyTableModel();
        table = new JTable(tableModel);
        table.addMouseListener(new CustomMouseListener());
        jScrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        setUpTypeCol();
        table.setDefaultRenderer(Icon.class, new CellRenderer());

        table.getColumnModel().getColumn(7).setPreferredWidth(200);
        table.getColumnModel().getColumn(8).setPreferredWidth(200);

        images = new ArrayList<>();

        String[] imgNames = {"skaler_begge", "skaler_horisontalt", "skaler_ingen", "skaler_vertikalt"};
        comboImages(4, imgNames, 7);
        String[] imgNames1 = {"anchor_center", "anchor_east",
                "anchor_north", "anchor_northeast", "anchor_northwest", "anchor_south",
                "anchor_southeast", "anchor_southwest", "anchor_west",};
        comboImages(9, imgNames1, 8);
    }

    /**
     * gets the jScrollpane for use in a panel.
     * @return the jScrollpane object.
     */
    public JScrollPane getJScrollPane() {
        return jScrollPane;
    }

    /**
     * Moves the rows in the table up or down. Calls the corresponding function
     * in the tableModel if the row to move is not the first or the last depending on
     * the direction to move the row. Then moves the the highlight and currently selected row variable
     * to match the move in the model.
     * @param rotation the int specifying the direction to move the row.
     */
    public void moveRows(int rotation) {
        ListSelectionModel mod = table.getSelectionModel();

        // if direction to move is up and the row to move is not the first
        if ((rotation == -1) && (selectedRow != 0)) {
            tableModel.moveRowsUp(selectedRow);
            mod.setSelectionInterval(selectedRow-1, selectedRow -1);
            selectedRow--;
        }
        // if direction to move is down and user is not on the last row
        else if ((rotation == 1) && (selectedRow != tableModel.getRowCount() -1)){
            tableModel.moveRowsDown(selectedRow);
            mod.setSelectionInterval(selectedRow+1, selectedRow +1);
            selectedRow++;
        }
        table.repaint();
    }

    /**
     * Gets all the rows from the table model.
     * @return a arraylist of strings where a individual string is a cell value
     */
    public ArrayList<String[]> getAllRows() {
        return tableModel.getAllValues();
    }

    /**
     * Passes a list of rows to the model.
     * @param list a list containg row objects.
     */
    public void loadTable(List list) {
       tableModel.setList(list);
    }

    /**
     * Removes all rows in the model.
     */
    public void clear() { tableModel.removeAll(); }

    /**
     * Gets the number of rows in the table model.
     */
    public int rowCount () {return table.getRowCount();}

    /**
     * creates a new row in the table model.
     */
    public void addRow() {
        tableModel.addRow();
    }

    /**
     * Removes a row from the table model.
     * @param row the row to be removed.
     */
    public void removeRow(int row) {
        tableModel.removeRow(row);
    }

    /**
     * Gets a list of all the row objects in the model.
     * @return the row list
     */
    public List getList() {
        return tableModel.getRowList();
    }

    /**
     * Sets up the comboBox for the type col.
     */
    private void setUpTypeCol() {
        JComboBox comboBox = new JComboBox();
        comboBox.addItem(Constants.JLABEL);
        comboBox.addItem(Constants.JBUTTON);
        comboBox.addItem(Constants.JTEXTAREA);
        comboBox.addItem(Constants.JTEXTFIELD);
        comboBox.setVisible(true);
        TableColumn typeCol = table.getColumnModel().getColumn(0);
        typeCol.setCellEditor(new DefaultCellEditor(comboBox));

        //Set up tool tips for the type cells.
        DefaultTableCellRenderer renderer =
                new DefaultTableCellRenderer();
        renderer.setToolTipText(messages.getString("rendererToolTip"));
        typeCol.setCellRenderer(renderer);
    }

    /**
     * The function that loads the images into an array, creates the comboBox
     */
    private void comboImages(int imgNum, String[] imgNames, int col) {
        imageNames = imgNames;
        ImageIcon[] icon = new ImageIcon[imgNum];
        Integer[] intArray = new Integer[imgNum];

        for (int i = 0; i < imgNum; i++) {
            intArray[i] = new Integer(i);

            URL resource = getClass().getResource("icons/" + imgNames[i] + ".png");
            icon[i] = new ImageIcon(resource);
            if (icon[i] != null) {
                icon[i].setDescription(imgNames[i]);
            }
        }
        // adds the imageArray to the arrayList
        images.add(icon);
        JComboBox comboBox = new JComboBox(intArray);
        ComboBoxRenderer renderer = new ComboBoxRenderer(col);
        renderer.setPreferredSize(new Dimension(100, 60));
        comboBox.setRenderer(renderer);
        comboBox.setMaximumRowCount(3);
        comboBox.setVisible(true);

        TableColumn typeCol = table.getColumnModel().getColumn(col);
        typeCol.setCellEditor(new DefaultCellEditor(comboBox));
    }

    /**
     * This class contains the functionality that lets the user right click a row and receive a little menu.
     * The menu lets the user either delete the selected row, or open an editor for the row.
     */
    class CustomMouseListener implements MouseListener {
        public void mouseClicked(MouseEvent e) {
        }
        //When the mouse button is pressed execute following code
        public void mousePressed(MouseEvent e) {
            selectedRow = table.getSelectedRow();

            //Check if it was the right mouse button and find the row on the point where they clicked
            if ( SwingUtilities.isRightMouseButton(e)){
                Point point = e.getPoint();
                int row = table.rowAtPoint(point);

                // Select the row clicked on
                ListSelectionModel model = table.getSelectionModel();
                model.setSelectionInterval(row, row);

                if (selectedRow <= table.getRowCount() && selectedRow != -1) {

                    JPopupMenu popupMenu;
                    JMenuItem itemEdit;
                    JMenuItem itemDelete;
                    popupMenu = new JPopupMenu("Popupmenu");

                    itemEdit = new JMenuItem(messages.getString("editProperties"));
                    itemDelete = new JMenuItem(messages.getString("removeRow"));

                    //Open the property editor when itemEdit is clicked
                    itemEdit.addActionListener(new ActionListener(){
                        public void actionPerformed(ActionEvent event) {
                            PropertyEditor.getProperties((String)tableModel.getValueAt(selectedRow, 0),
                                    tableModel, selectedRow, internationalization);
                        }
                    });

                    //Delete selected row when itemDelete is clicked.
                    itemDelete.addActionListener(new ActionListener(){
                        public void actionPerformed(ActionEvent ev) {
                            removeRow(selectedRow);
                            model.clearSelection();
                            table.repaint();
                        }
                    });

                    popupMenu.add(itemEdit);
                    popupMenu.add(itemDelete);

                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
                    
                }
            }
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }
    }


    /**
     * Class for rendering a list of images inside the comboBox dropdown.
     */
    private class ComboBoxRenderer extends JLabel implements ListCellRenderer {
        private int col;

        /**
         * Constructor for the renderer
         * @param col the column the renderes is going to render
         */
        public ComboBoxRenderer(int col) {
            this.col = col;
            setOpaque(true);
            setHorizontalAlignment(CENTER);
            setVerticalAlignment(CENTER);
        }

        /**
         * loads the image from the imagearray and sets the icon.
         */
        public Component getListCellRendererComponent(
                JList list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus) {

            int selectedIndex = (Integer)value;

            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }

            // gets the index of the imagearray to render depending on the column number
            int type;
            if (col == 7)
               type = 0;
            else
                type = 1;

            ImageIcon icon = images.get(type)[selectedIndex];
            String name = imageNames[selectedIndex];
            setIcon(icon);
            if (icon != null) {
                setText(name);
                setFont(list.getFont());
            } else {
                System.out.println("no image available");
            }
            return this;
        }
    }

    /**
     * Class for rendering the columns containing the images
     */
    class CellRenderer extends DefaultTableCellRenderer {

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int col) {
            // if else for getting the correct image array from the arrayList
            if (col == 7)
                col = 0;
            else
                col = 1;

            setIcon(images.get(col)[Integer.valueOf(value.toString())]);

            return this;
        }
    }
}

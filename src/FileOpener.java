import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.FileFilter;
import java.util.ResourceBundle;

/**
 * Defines java explorer layout for opening and saving files
 * @author Kristian, Per-Kristian
 * Date: 02.10.2015
 */
public class FileOpener {
    JFileChooser fileChooser;
    Internationalization internationalization;
    ResourceBundle messages;
    FileNameExtensionFilter jarFilter;
    FileNameExtensionFilter ezlFilter;

    /**
     * This constructor is only used for initializing / defining variables.
     * @param internationalization object for internationalization
     */
    public FileOpener(Internationalization internationalization) {
        this.internationalization = internationalization;
        messages = internationalization.getLang();
        fileChooser = new JFileChooser();
        jarFilter = new FileNameExtensionFilter(messages.getString("javaFormat"), "java", "jar");
        ezlFilter = new FileNameExtensionFilter(messages.getString("fileFormat"), "ezl");
    }

    /**
     * This function creates an Open-dialog in the FileChooser, and lets the user choose a file. If they select a
     * file with an illegal file type, an error message is displayed.
     * @return the path of the file
     */
    public String open() {
        // Sets appropriate file filter and selection mode
        fileChooser.removeChoosableFileFilter(jarFilter);
        fileChooser.setFileFilter(ezlFilter);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        // Shows the open-dialog and gets the path of the selected file
        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file1 = fileChooser.getSelectedFile();
            // Checks if the file type is legal
            if (file1.getName().toLowerCase().endsWith(".ezl")) {
                return file1.getAbsolutePath();
            }
            else {
                JOptionPane.showMessageDialog(null, messages.getString("fileOpenFail2"),
                        messages.getString("fileOpenFail1"), JOptionPane.ERROR_MESSAGE);
            }
        }
        return null;
    }

    /**
     * This function provides the user with a save-dialog. It takes a type parameter, which specifies whether the file
     * to save is a java file, or a layout file.
     * @param type the type of file to save
     * @return the path of the file to save
     */
    public String save(String type) {
        if (type == "java") {
            fileChooser.removeChoosableFileFilter(ezlFilter);
            fileChooser.setFileFilter(jarFilter);
        }
        else {
            fileChooser.removeChoosableFileFilter(jarFilter);
            fileChooser.setFileFilter(ezlFilter);
        }
        // Sets the proper selection mode, and sets a default filename in the save-dialog
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setSelectedFile(new File(messages.getString("fileName")));

        // Gets the path of the file and returns it
        if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
                return fileToSave.getAbsolutePath();
        }
        return null;
    }
}

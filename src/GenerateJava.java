import com.sun.codemodel.*;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This class contains methods for generating javaCode to a file.
 * @author Jardar Tøn
 * Date: 02.10.2015
 */
public class GenerateJava {

    /**
     * This function does all the code generation and writes to the file.
     * @param rows List of all rows. Each row is a array containing all its data
     * @param name path and name of the file to write to
     */
    public static void MakeCode(ArrayList<String[]> rows, String name) {
        JCodeModel codeModel = new JCodeModel();

        try {

            //default path
            String path = "./";

            //generates a preview name if name is null. name is null if a preview has been requested.
            if (name == null) {
                name = "preview" + Long.toString(System.currentTimeMillis());
            }
            //if path and name is sendt this is split up to path and name. the folderDelim is for win and unix
            else {
                String folderDelim = "/";
                if (!name.contains(folderDelim))
                    folderDelim = "\\";

                path = name.substring(0, name.lastIndexOf(folderDelim));
                name = name.substring(name.lastIndexOf(folderDelim) + 1);
            }

            //creates the class
            JDefinedClass classToCreate = codeModel._class(name);
            JClass jpanel = codeModel.ref(JPanel.class);
            classToCreate._extends(jpanel);

            // creates the class variables and initalizes them
            for (String[] row : rows) {

                String className = "javax.swing." + row[0];

                // if the constructor has one additional param. can happen in Jtexfield
                if (((row.length > 9) && (row.length <11 )) && (row[0] == Constants.JTEXTFIELD)) {
                    JVar var = classToCreate.field(JMod.PRIVATE,
                            Class.forName(className), row[1]).
                            init(JExpr.direct("new " + row[0] + "(\"" + row[2] + "\", "+ row[9]+")"));
                }
                // if the constructor has two additional paramaters. for JTextarea
                else if ((row.length > 10) && (row[0] == Constants.JTEXTAREA)) {
                    JVar var = classToCreate.field(JMod.PRIVATE,
                            Class.forName(className), row[1]).
                            init(JExpr.direct("new " + row[0] + "(\"" + row[2] + "\", "+ row[9]+", "+row[10]+")"));
                }
                else {
                    JVar var = classToCreate.field(JMod.PRIVATE,
                            Class.forName(className), row[1]).
                            init(JExpr.direct("new " + row[0] + "(\"" + row[2] + "\")"));
                }
            }

                JMethod constructorMethod = classToCreate.constructor(JMod.PUBLIC);

                JBlock constructorBlock = constructorMethod.body();

                JVar layout = constructorBlock
                        .decl(codeModel._ref(GridBagLayout.class), "layout").init(JExpr.direct("new GridBagLayout()"));

                JVar gbc = constructorBlock
                        .decl(codeModel._ref(GridBagConstraints.class), "gbc").init(JExpr.direct("new GridBagConstraints()"));

                constructorBlock.directStatement("setLayout(layout);");

            // writes code for each element
            for (String[] row : rows) {

                constructorBlock.directStatement("gbc.gridx = " + row[3] + ";");
                constructorBlock.directStatement("gbc.gridy = " + row[4] + ";");
                constructorBlock.directStatement("gbc.gridwidth = " + row[5] + ";");
                constructorBlock.directStatement("gbc.gridheight =" + row[6] + ";");
                constructorBlock.directStatement("gbc.anchor = java.awt.GridBagConstraints."
                        + getAnchor(Integer.valueOf(row[7])) + ";");
                constructorBlock.directStatement("gbc.fill= java.awt.GridBagConstraints." +
                        getFill(Integer.valueOf(row[8])) + ";");
                constructorBlock.directStatement("layout.setConstraints(" + row[1] + ", gbc);");
                constructorBlock.directStatement("add(" + row[1] + ");");
            }

            // if file is not a preview. then make a main method.
            if (name != null) {

                JMethod mainMethod = classToCreate.method(JMod.PUBLIC + JMod.STATIC, void.class, "main");
                mainMethod.param(String[].class, "args");
                JBlock mainBody = mainMethod.body();
                JVar frame = mainBody
                        .decl(codeModel._ref(JFrame.class), "frame").init(JExpr.direct("new JFrame()"));
                mainBody.directStatement(name +" panel = new " +name+ "();");
                mainBody.directStatement("frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);");
                mainBody.directStatement("frame.add(panel);");
                mainBody.directStatement("frame.pack();");
                mainBody.directStatement("frame.setVisible(true);");
            }

            // create the file
            File file = new File(path);
            file.mkdirs();
            codeModel.build(file);

        } catch (JClassAlreadyExistsException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
             ex.printStackTrace();
        }
    }

    /**
     * gets the corresponding string value to the int for the gridBagConstraintsLayout.anchor
     * @param anchor the int value from the table.
     * @return The string to be put in the output code.
     */
    public static String getAnchor(int anchor) {
       switch (anchor) {
           case 0: return "CENTER";
           case 1: return "EAST";
           case 2: return "NORTH";
           case 3: return "NORTHEAST";
           case 4: return "NORTHWEST";
           case 5: return "SOUTH";
           case 6: return "SOUTHEAST";
           case 7: return "SOUTHWEST";
           case 8: return "WEST";
           default: return "CENTER";
       }
    }

    /**
     * gets the corresponding string value to the int for the gridBagConstraintsLayout.fill
     * @param fill the int value from the table.
     * @return The string to be put in the output code.
     */
    public static String getFill(int fill) {
        switch (fill) {
            case 1: return "BOTH";
            case 2: return "HORIZONTAL";
            case 3: return "NONE";
            case 4: return "VERTICAL";
            default: return "NONE";
        }
    }
}

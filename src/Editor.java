import javax.swing.*;
import java.awt.*;

/**
 * Created by Kristian on 30.09.2015.
 * Includes main and editor that builds the main layout with jframe, jpanel and menus
 * @author Kristian
 * @date 30.09.2015
 */

public class Editor extends JFrame{
    private JPanel panel;
    private BorderLayout border;
    private TopBarMenu topBarMenu;
    private ToolBar toolBar;
    public Internationalization internationalization;
    public Table table;
    public String currentFile = "";


    public Editor() {
        super("Ez-Editor");

        // Defines language from OS
        internationalization = new Internationalization(System.getProperty("user.language"), System.getProperty("user.country"));
        panel = new JPanel();
        border = new BorderLayout();

        //Creating new table
        table = new Table(internationalization);

        // Creates toolbar and navbar
        topBarMenu = new TopBarMenu(this, border, panel, internationalization, table);
        toolBar = new ToolBar(this, border, panel, internationalization, table);

        // Sets toolbar, navbar and table
        this.setJMenuBar(topBarMenu.createTopBarMenu());
        panel.setLayout(border);
        panel.add("North", toolBar.createToolBar());
        panel.add("Center", table.getJScrollPane());
        add(panel);
    }

    // Creates one editor object and scales it according to its content. Also include L&F
    public static void main(String[] args){
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (InstantiationException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Editor editor = new Editor();
        editor.setPreferredSize(new Dimension(800, 600));
        editor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        editor.pack();
        editor.setVisible(true);
    }
}

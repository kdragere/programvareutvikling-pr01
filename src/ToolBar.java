import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.List;

/**
 * Created by Kristian on 02.10.2015.
 * @author Kristian, Per-Kristian
 * This class defines the toolbar in the main layout. The toolbaer is filled wuth buttons.
 * Every button have a actionlistener.
 */
public class ToolBar {
    Internationalization internationalization;
    ResourceBundle messages;
    FileOpener fileOpener;
    BorderLayout border;
    Editor editor;
    JPanel panel;
    Table table;

    public ToolBar(Editor editor, BorderLayout border, JPanel panel, Internationalization internationalization,
                   Table table) {
        this.editor = editor;
        this.border = border;
        this.panel = panel;
        this.internationalization = internationalization;
        this.table = table;
    }

    public JToolBar createToolBar() {
        messages = internationalization.getLang();
        fileOpener = new FileOpener(internationalization);

        JToolBar toolBar = new JToolBar();
        toolBar.setRollover(true);
        URL newUrl = getClass().getResource("icons/NEW.GIF");
        JButton newButton = new JButton(new ImageIcon(newUrl));
        newButton.setToolTipText(messages.getString("newButton"));
        newButton.addActionListener(new ActionListener() {
            /**
             * When the user clicks the New, a blank table will appear.
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.table.clear();
                panel.repaint();
            }
        });
        toolBar.add(newButton);

        URL openUrl = getClass().getResource("icons/OPENDOC.GIF");
        JButton openButton = new JButton(new ImageIcon(openUrl));

        openButton.setToolTipText(messages.getString("openButton"));
        openButton.addActionListener(new ActionListener() {
            /**
             * When the user clicks the openButton, a FileChooser will be displayed, letting the user choose which
             * file to load.
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                //Open a FileChooser and load the selected file.
                String fileName = fileOpener.open();
                if (fileName != null) {
                    editor.table.loadTable((List) LoadAndSave.loadFromFile(fileName));
                    panel.repaint();
                }
            }
        });
        toolBar.add(openButton);

        URL saveUrl = getClass().getResource("icons/SAVE.GIF");
        JButton saveButton = new JButton(new ImageIcon(saveUrl));

        saveButton.setToolTipText(messages.getString("saveButton"));
        saveButton.addActionListener(new ActionListener() {
            /**
             * When the user clicks the saveButton, and they are working on an existing file, it will overwrite that
             * file. If they are working on a new layout, the button works as a "Save as.." button.
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                // If there's a file currently open, save to that file
                if (!editor.currentFile.equals("")) {
                    LoadAndSave.saveToFile(editor.currentFile, editor.table.getList());
                } else {
                    // Opens a FileChooser for saving, and updates currentFile.
                    String fileName = fileOpener.save(null);
                    if (fileName != null) {
                        fileName = fileName + ".ezl";
                        LoadAndSave.saveToFile(fileName, editor.table.getList());
                        editor.currentFile = fileName;
                    }
                }
            }
        });
        toolBar.add(saveButton);
        toolBar.addSeparator();

        URL execUrl = getClass().getResource("icons/ExecuteProject.gif");
        JButton execButton = new JButton(new ImageIcon(execUrl));

        execButton.setToolTipText(messages.getString("execButton"));
        execButton.addActionListener(new ActionListener() {
            /**
             * When the user clicks the executeButton, the preview of code will appear.
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                GenerateJava.MakeCode(editor.table.getAllRows(), null);
            }
        });
        toolBar.add(execButton);

        URL saveJavaUrl = getClass().getResource("icons/SAVEJAVA.GIF");
        JButton saveJavaButton = new JButton(new ImageIcon(saveJavaUrl));

        saveJavaButton.setToolTipText(messages.getString("saveJavaButton"));
        saveJavaButton.addActionListener(new ActionListener() {
            /**
             * When the user clicks the saveJavaButton, javaCode will be generated and save to file
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                String fileName = fileOpener.save("java");
                if (fileName != null) {
                    GenerateJava.MakeCode(editor.table.getAllRows(), fileName);
                }
            }
        });
        toolBar.add(saveJavaButton);
        toolBar.addSeparator();

        URL newRowUrl= getClass().getResource("icons/NEWROW.GIF");
        JButton newRowButton = new JButton(new ImageIcon(newRowUrl));

        newRowButton.setToolTipText(messages.getString("newRowButton"));
        newRowButton.addActionListener(new ActionListener() {
            /**
             * When the user clicks the newRowButton, a new row will be added to the table.
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.table.addRow();
                panel.repaint();
            }
        });
        toolBar.add(newRowButton);

        URL deleteRowUrl = getClass().getResource("icons/close.gif");
        JButton deleteRowButton = new JButton(new ImageIcon(deleteRowUrl));

        deleteRowButton.setToolTipText(messages.getString("menuChangeDeleteRow"));
        deleteRowButton.addActionListener(new ActionListener() {
            /**
             * When the user clicks the delteRowButton, selected row will we deleted from table
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if (editor.table.rowCount() > 0 && editor.table.selectedRow > -1
                        && editor.table.selectedRow < editor.table.rowCount()) {
                    editor.table.removeRow(table.selectedRow);
                    panel.repaint();
                }
            }
        });
        toolBar.add(deleteRowButton);

        URL moveUpUrl = getClass().getResource("icons/MoveRowUp.gif");
        JButton moveUpButton = new JButton(new ImageIcon(moveUpUrl));

        moveUpButton.setToolTipText(messages.getString("moveUpButton"));
        moveUpButton.addActionListener(new ActionListener() {
            /**
             * When the user clicks the moveUpButton, table rotates
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.table.moveRows(-1);
            }
        });
        toolBar.add(moveUpButton);

        URL moveDownUrl = getClass().getResource("icons/MoveRowDown.gif");
        JButton moveDownButton = new JButton(new ImageIcon(moveDownUrl));

        moveDownButton.setToolTipText(messages.getString("moveDownButton"));
        moveDownButton.addActionListener(new ActionListener() {
            /**
             * When the user clicks the moveDownButton, table rotates
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.table.moveRows(1);
            }
        });
        toolBar.add(moveDownButton);
        toolBar.addSeparator();

        URL helpUrl = getClass().getResource("icons/HELP.GIF");
        JButton helpButton = new JButton(new ImageIcon(helpUrl));

        helpButton.setToolTipText(messages.getString("helpButton"));
        helpButton.addActionListener(new ActionListener() {
            /**
             * When the user clicks the helpButton, help appears
             * @param e the action performed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, messages.getString("menuHelpHelpTxt"), messages.getString("menuHelpHelp"), JOptionPane.INFORMATION_MESSAGE);
            }
        });
        toolBar.add(helpButton);

        return toolBar;
    }
}